EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	3175 3325 3175 2800
Text Label 9625 3625 2    50   ~ 0
motor_Ap
Text Label 9625 3925 2    50   ~ 0
motor_Am
Text Label 9625 3525 2    50   ~ 0
motor_Bp
Text Label 9625 3225 2    50   ~ 0
motor_Bm
$Comp
L Device:C C2
U 1 1 5EBFD583
P 6800 4325
F 0 "C2" V 6975 4325 50  0000 C CNN
F 1 "0.22uF" V 7075 4325 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6838 4175 50  0001 C CNN
F 3 "~" H 6800 4325 50  0001 C CNN
	1    6800 4325
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5EC03CD9
P 8675 4475
F 0 "#PWR0102" H 8675 4225 50  0001 C CNN
F 1 "GND" H 8680 4302 50  0000 C CNN
F 2 "" H 8675 4475 50  0001 C CNN
F 3 "" H 8675 4475 50  0001 C CNN
	1    8675 4475
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5EBFBEB5
P 7175 3325
F 0 "C3" V 6925 3325 50  0000 C CNN
F 1 "0.22uF" V 7025 3325 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7213 3175 50  0001 C CNN
F 3 "~" H 7175 3325 50  0001 C CNN
	1    7175 3325
	0    1    1    0   
$EndComp
$Comp
L fab:STEPPER_DRIVER_A4982 U3
U 1 1 5EC35540
P 8675 3575
F 0 "U3" H 8675 4390 50  0000 C CNN
F 1 "STEPPER_DRIVER_A4982" H 8675 4299 50  0000 C CNN
F 2 "fab:24-TSSOP-EP" H 8675 3575 50  0001 C CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/A4982-Datasheet.ashx" H 8675 3575 50  0001 C CNN
	1    8675 3575
	1    0    0    -1  
$EndComp
Wire Wire Line
	9225 3625 9625 3625
Wire Wire Line
	9225 3525 9625 3525
Wire Wire Line
	9225 3225 9625 3225
Wire Wire Line
	9225 3925 9625 3925
Wire Wire Line
	10000 3825 9225 3825
Wire Wire Line
	10000 4125 10000 4325
Wire Wire Line
	10000 4325 9225 4325
Wire Wire Line
	8675 4475 8675 4325
Connection ~ 8675 4325
$Comp
L Device:R R9
U 1 1 5EC60193
P 10575 3725
F 0 "R9" V 10368 3725 50  0000 C CNN
F 1 "0.1" V 10459 3725 50  0000 C CNN
F 2 "fab:R_1206" V 10505 3725 50  0001 C CNN
F 3 "~" H 10575 3725 50  0001 C CNN
	1    10575 3725
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5EC619A9
P 10575 3425
F 0 "R8" V 10368 3425 50  0000 C CNN
F 1 "0.1" V 10459 3425 50  0000 C CNN
F 2 "fab:R_1206" V 10505 3425 50  0001 C CNN
F 3 "~" H 10575 3425 50  0001 C CNN
	1    10575 3425
	0    1    1    0   
$EndComp
Wire Wire Line
	9225 3425 10425 3425
Wire Wire Line
	9225 3725 10425 3725
Wire Wire Line
	10725 3425 10725 3725
$Comp
L Device:C C6
U 1 1 5EC696C6
P 10000 3975
F 0 "C6" H 10125 3950 50  0000 L CNN
F 1 "100uF" H 10050 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10038 3825 50  0001 C CNN
F 3 "~" H 10000 3975 50  0001 C CNN
	1    10000 3975
	1    0    0    -1  
$EndComp
Wire Wire Line
	10725 3725 10725 4325
Wire Wire Line
	10725 4325 10300 4325
Connection ~ 10725 3725
Wire Wire Line
	8125 3325 7325 3325
Wire Wire Line
	10000 3325 10000 3825
Wire Wire Line
	9225 3325 10000 3325
Connection ~ 10000 3825
Wire Wire Line
	9225 4025 9625 4025
Text Label 9625 4025 2    50   ~ 0
DIR
Text Label 6650 2250 0    50   ~ 0
5V
$Comp
L Device:C C5
U 1 1 5ECD5765
P 7900 3025
F 0 "C5" V 7648 3025 50  0000 C CNN
F 1 "0.1uF" V 7739 3025 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7938 2875 50  0001 C CNN
F 3 "~" H 7900 3025 50  0001 C CNN
	1    7900 3025
	0    1    1    0   
$EndComp
Wire Wire Line
	7750 3025 7750 3125
Wire Wire Line
	7750 3125 8125 3125
Wire Wire Line
	8125 3025 8050 3025
$Comp
L Device:C C4
U 1 1 5ECDDAF1
P 7650 2900
F 0 "C4" H 7425 3100 50  0000 L CNN
F 1 "0.1uF" H 7375 3000 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 7688 2750 50  0001 C CNN
F 3 "~" H 7650 2900 50  0001 C CNN
	1    7650 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2625 10000 2625
Wire Wire Line
	10000 2625 10000 3325
Connection ~ 10000 3325
Wire Wire Line
	8125 4025 7600 4025
Wire Wire Line
	8125 3425 7600 3425
Wire Wire Line
	8125 3525 7600 3525
Wire Wire Line
	8125 3625 7600 3625
Wire Wire Line
	8125 3725 7025 3725
Text Label 7600 4025 0    50   ~ 0
STEP
Text Label 5775 3825 0    50   ~ 0
SLEEP
Text Label 7600 3625 0    50   ~ 0
RESET
Text Label 7600 3525 0    50   ~ 0
MS2
Text Label 7600 3425 0    50   ~ 0
MS1
Wire Wire Line
	9225 3125 9625 3125
Wire Wire Line
	9225 3025 10725 3025
Wire Wire Line
	10725 3025 10725 3425
Connection ~ 10725 3425
Text Label 9625 3125 2    50   ~ 0
ENABLE
Wire Wire Line
	7650 2625 7650 2750
Wire Wire Line
	7650 3050 7650 3225
Wire Wire Line
	7650 3225 8125 3225
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2
U 1 1 5ED14038
P 8700 1025
F 0 "J2" H 8750 1242 50  0000 C CNN
F 1 "Stepper_Conn" H 8750 1151 50  0000 C CNN
F 2 "fab:fab-2X02SMD" H 8700 1025 50  0001 C CNN
F 3 "~" H 8700 1025 50  0001 C CNN
	1    8700 1025
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 1025 8025 1025
Wire Wire Line
	8500 1125 8025 1125
Wire Wire Line
	9000 1025 9475 1025
Wire Wire Line
	9000 1125 9475 1125
Text Label 8025 1025 0    50   ~ 0
motor_Ap
Text Label 8025 1125 0    50   ~ 0
motor_Am
Text Label 9475 1025 2    50   ~ 0
motor_Bp
Text Label 9475 1125 2    50   ~ 0
motor_Bm
Wire Wire Line
	10000 2625 10000 2325
Text Label 10000 2325 0    50   ~ 0
9V
Wire Wire Line
	8125 4125 7600 4125
$Comp
L Regulator_Linear:LM3480-5.0 U1
U 1 1 5ED4A087
P 3225 1100
F 0 "U1" H 3225 1342 50  0000 C CNN
F 1 "LM3480-5.0" H 3225 1251 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3225 1325 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm3480.pdf" H 3225 1100 50  0001 C CNN
	1    3225 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5ED4A8A6
P 3575 1525
F 0 "#PWR0103" H 3575 1275 50  0001 C CNN
F 1 "GND" H 3580 1352 50  0000 C CNN
F 2 "" H 3575 1525 50  0001 C CNN
F 3 "" H 3575 1525 50  0001 C CNN
	1    3575 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	2925 1100 2600 1100
Text Label 2600 1100 0    50   ~ 0
9V
Text Label 4075 1100 2    50   ~ 0
5V
Text Label 3175 2350 0    50   ~ 0
5V
$Comp
L fab:ATtiny1614 U2
U 1 1 5ED51B29
P 3775 3775
F 0 "U2" H 3775 4542 50  0000 C CNN
F 1 "ATtiny1614" H 3775 4451 50  0000 C CNN
F 2 "fab:fab-SOIC-14_3.9x8.7mm_P1.27mm" H 3775 3775 50  0001 C CIN
F 3 "" H 3775 3775 50  0001 C CNN
	1    3775 3775
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5ED55BEB
P 3725 2800
F 0 "C1" V 3473 2800 50  0000 C CNN
F 1 "1uF" V 3564 2800 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3763 2650 50  0001 C CNN
F 3 "~" H 3725 2800 50  0001 C CNN
	1    3725 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	3875 2800 4525 2800
Wire Wire Line
	4525 2800 4525 3325
Wire Wire Line
	3575 2800 3175 2800
Connection ~ 3175 2800
Wire Wire Line
	3175 2800 3175 2350
$Comp
L fab:FTDI-SMD-HEADER M1
U 1 1 5ED621FE
P 1550 2450
F 0 "M1" H 1550 2450 45  0001 C CNN
F 1 "FTDI-SMD-HEADER" H 1550 2450 45  0001 C CNN
F 2 "custom-footprints:FTDI_Header_1x06_P2.54mm_Horizontal" H 1580 2600 20  0001 C CNN
F 3 "" H 1550 2450 50  0001 C CNN
	1    1550 2450
	-1   0    0    1   
$EndComp
$Comp
L fab:UPDI-SMD-HEADER M2
U 1 1 5ED63FBC
P 1575 3100
F 0 "M2" H 1575 3100 45  0001 C CNN
F 1 "UPDI-SMD-HEADER" H 1575 3100 45  0001 C CNN
F 2 "custom-footprints:UPDI_Header_1x02_P2.54mm_Horizontal" H 1605 3250 20  0001 C CNN
F 3 "" H 1575 3100 50  0001 C CNN
	1    1575 3100
	-1   0    0    1   
$EndComp
Wire Wire Line
	1575 3500 1925 3500
Wire Wire Line
	1575 3600 1925 3600
Text Label 1925 3600 2    50   ~ 0
UPDI
Text Label 1925 3500 2    50   ~ 0
GND
Wire Wire Line
	1550 2950 1925 2950
Wire Wire Line
	1550 2750 1925 2750
Wire Wire Line
	1550 2650 1925 2650
Wire Wire Line
	1550 2550 1925 2550
NoConn ~ 1550 2450
NoConn ~ 1550 2850
Text Label 1925 2950 2    50   ~ 0
GND
Text Label 1925 2650 2    50   ~ 0
RX
Text Label 1925 2550 2    50   ~ 0
TX
Text Label 2425 2750 2    50   ~ 0
5V
$Comp
L Device:LED D3
U 1 1 5ED891F5
P 4750 850
F 0 "D3" H 4743 595 50  0000 C CNN
F 1 "PWR_LED" H 4743 686 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 4750 850 50  0001 C CNN
F 3 "~" H 4750 850 50  0001 C CNN
	1    4750 850 
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 5ED891FF
P 5175 850
F 0 "R2" V 4968 850 50  0000 C CNN
F 1 "499" V 5059 850 50  0000 C CNN
F 2 "fab:R_1206" V 5105 850 50  0001 C CNN
F 3 "~" H 5175 850 50  0001 C CNN
	1    5175 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	4900 850  5025 850 
Wire Wire Line
	5325 850  5650 850 
Wire Wire Line
	4600 850  4300 850 
Text Label 4300 850  0    50   ~ 0
5V
Text Label 5650 850  2    50   ~ 0
GND
$Comp
L Device:LED D5
U 1 1 5ED8C9EA
P 6325 875
F 0 "D5" H 6318 620 50  0000 C CNN
F 1 "RX_LED" H 6318 711 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6325 875 50  0001 C CNN
F 3 "~" H 6325 875 50  0001 C CNN
	1    6325 875 
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 5ED8C9F4
P 6750 875
F 0 "R4" V 6543 875 50  0000 C CNN
F 1 "499" V 6634 875 50  0000 C CNN
F 2 "fab:R_1206" V 6680 875 50  0001 C CNN
F 3 "~" H 6750 875 50  0001 C CNN
	1    6750 875 
	0    1    1    0   
$EndComp
Wire Wire Line
	6475 875  6600 875 
Wire Wire Line
	6900 875  7225 875 
Wire Wire Line
	6175 875  5875 875 
Text Label 5875 875  0    50   ~ 0
RX
Text Label 7225 875  2    50   ~ 0
GND
$Comp
L Device:LED D4
U 1 1 5ED909B3
P 6350 1250
F 0 "D4" H 6343 995 50  0000 C CNN
F 1 "TX_LED" H 6343 1086 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6350 1250 50  0001 C CNN
F 3 "~" H 6350 1250 50  0001 C CNN
	1    6350 1250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5ED909BD
P 6775 1250
F 0 "R3" V 6568 1250 50  0000 C CNN
F 1 "499" V 6659 1250 50  0000 C CNN
F 2 "fab:R_1206" V 6705 1250 50  0001 C CNN
F 3 "~" H 6775 1250 50  0001 C CNN
	1    6775 1250
	0    1    1    0   
$EndComp
Wire Wire Line
	6500 1250 6625 1250
Wire Wire Line
	6925 1250 7250 1250
Wire Wire Line
	6200 1250 5900 1250
Text Label 5900 1250 0    50   ~ 0
TX
Text Label 7250 1250 2    50   ~ 0
GND
$Comp
L Device:LED D6
U 1 1 5ED9405B
P 6325 1700
F 0 "D6" H 6318 1445 50  0000 C CNN
F 1 "LED" H 6318 1536 50  0000 C CNN
F 2 "LED_SMD:LED_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 6325 1700 50  0001 C CNN
F 3 "~" H 6325 1700 50  0001 C CNN
	1    6325 1700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5ED94065
P 6750 1700
F 0 "R5" V 6543 1700 50  0000 C CNN
F 1 "499" V 6634 1700 50  0000 C CNN
F 2 "fab:R_1206" V 6680 1700 50  0001 C CNN
F 3 "~" H 6750 1700 50  0001 C CNN
	1    6750 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	6475 1700 6600 1700
Wire Wire Line
	6900 1700 7225 1700
Wire Wire Line
	6175 1700 5875 1700
Text Label 5875 1700 0    50   ~ 0
LED
Text Label 7225 1700 2    50   ~ 0
GND
$Comp
L Switch:SW_Push SW1
U 1 1 5ED97E93
P 5000 1300
F 0 "SW1" H 5000 1585 50  0000 C CNN
F 1 "SW_Push" H 5000 1494 50  0000 C CNN
F 2 "custom-footprints:TACTILE_SW_B3S" H 5000 1500 50  0001 C CNN
F 3 "~" H 5000 1500 50  0001 C CNN
	1    5000 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 1300 5650 1300
Wire Wire Line
	4800 1300 4300 1300
Text Label 5650 1300 2    50   ~ 0
GND
Text Label 4300 1300 0    50   ~ 0
BUTTON
Wire Wire Line
	4375 3925 4900 3925
Text Label 4900 3925 2    50   ~ 0
UPDI
Wire Wire Line
	2225 2750 2425 2750
Wire Wire Line
	4375 3475 4900 3475
Wire Wire Line
	4375 3625 4900 3625
Wire Wire Line
	4375 3775 4900 3775
Wire Wire Line
	4375 4075 4900 4075
Wire Wire Line
	4375 4225 4900 4225
Wire Wire Line
	3175 3475 2650 3475
Wire Wire Line
	3175 3625 2650 3625
Wire Wire Line
	3175 3775 2650 3775
Wire Wire Line
	3175 3925 2650 3925
Wire Wire Line
	3175 4075 2650 4075
Wire Wire Line
	3175 4225 2650 4225
Text Label 2650 4225 0    50   ~ 0
TX
Text Label 2650 4075 0    50   ~ 0
RX
Text Label 2650 3475 0    50   ~ 0
BUTTON
Text Label 2650 3625 0    50   ~ 0
LED
Connection ~ 10000 2625
Text Label 4900 4075 2    50   ~ 0
RESET
Text Label 4900 4225 2    50   ~ 0
SLEEP
Text Label 2650 3775 0    50   ~ 0
DIR
Text Label 2650 3925 0    50   ~ 0
STEP
Text Label 4900 3475 2    50   ~ 0
ENABLE
Text Label 4900 3625 2    50   ~ 0
MS1
Text Label 4900 3775 2    50   ~ 0
MS2
Wire Wire Line
	4375 3325 4525 3325
Text Label 4900 3325 2    50   ~ 0
GND
Wire Wire Line
	3950 1100 4075 1100
Connection ~ 4525 3325
Wire Wire Line
	4525 3325 4900 3325
Wire Wire Line
	7025 3325 7025 3725
Connection ~ 7025 3725
$Comp
L fab:R_POT_10K_Trimmer RV1
U 1 1 5EE4C252
P 7450 4125
F 0 "RV1" H 7380 4171 50  0000 R CNN
F 1 "R_POT_10K_Trimmer" V 7300 4100 50  0000 R CNN
F 2 "fab:Potentiometer_TT_Model-23_4.5x5.0x3.0mm" H 7450 4125 50  0001 C CNN
F 3 "https://www.ttelectronics.com/TTElectronics/media/ProductFiles/Trimmers/Datasheets/23.pdf" H 7450 4125 50  0001 C CNN
	1    7450 4125
	1    0    0    -1  
$EndComp
Wire Wire Line
	7025 4325 7450 4325
Wire Wire Line
	7450 4275 7450 4325
Connection ~ 7450 4325
Wire Wire Line
	7450 4325 8675 4325
Wire Wire Line
	7450 3975 7450 2925
$Comp
L Device:R R7
U 1 1 5EE8EDA4
P 6900 2925
F 0 "R7" V 6693 2925 50  0000 C CNN
F 1 "50k" V 6784 2925 50  0000 C CNN
F 2 "fab:R_1206" V 6830 2925 50  0001 C CNN
F 3 "~" H 6900 2925 50  0001 C CNN
	1    6900 2925
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 2250 6650 2925
Wire Wire Line
	7450 2925 7050 2925
Wire Wire Line
	6750 2925 6650 2925
Connection ~ 6650 2925
Wire Wire Line
	6650 2925 6650 3925
Wire Wire Line
	1600 950  1600 800 
Wire Wire Line
	1800 950  1600 950 
Wire Wire Line
	1600 1550 1600 1650
Connection ~ 1600 1550
Wire Wire Line
	1800 1550 1600 1550
$Comp
L power:GND #PWR0101
U 1 1 5EBF6970
P 1600 1650
F 0 "#PWR0101" H 1600 1400 50  0001 C CNN
F 1 "GND" H 1605 1477 50  0000 C CNN
F 2 "" H 1600 1650 50  0001 C CNN
F 3 "" H 1600 1650 50  0001 C CNN
	1    1600 1650
	1    0    0    -1  
$EndComp
Connection ~ 1600 950 
Wire Wire Line
	1600 1200 1600 950 
Wire Wire Line
	1600 1400 1600 1550
Wire Wire Line
	1600 1300 1600 1400
Connection ~ 1600 1400
$Comp
L Connector:Barrel_Jack_Switch J1
U 1 1 5EBF4FE0
P 1300 1300
F 0 "J1" H 1357 1617 50  0000 C CNN
F 1 "Barrel_Jack_Switch" V 1000 1300 50  0000 C CNN
F 2 "custom-footprints:Power_Barrel_Jack_PJ-002AH-SMT-TR" H 1350 1260 50  0001 C CNN
F 3 "~" H 1350 1260 50  0001 C CNN
	1    1300 1300
	1    0    0    -1  
$EndComp
Text Label 1600 800  0    50   ~ 0
9V
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5EBF6FEF
P 1800 950
F 0 "#FLG0101" H 1800 1025 50  0001 C CNN
F 1 "PWR_FLAG" V 1800 1078 50  0000 L CNN
F 2 "" H 1800 950 50  0001 C CNN
F 3 "~" H 1800 950 50  0001 C CNN
	1    1800 950 
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5EBF7A4E
P 1800 1550
F 0 "#FLG0102" H 1800 1625 50  0001 C CNN
F 1 "PWR_FLAG" V 1800 1678 50  0000 L CNN
F 2 "" H 1800 1550 50  0001 C CNN
F 3 "~" H 1800 1550 50  0001 C CNN
	1    1800 1550
	0    1    1    0   
$EndComp
Wire Wire Line
	3225 1400 3575 1400
Wire Wire Line
	3575 1525 3575 1400
Wire Wire Line
	9225 4125 9225 4325
Connection ~ 9225 4325
Wire Wire Line
	9225 4325 8675 4325
Connection ~ 10000 4325
$Comp
L Device:R R6
U 1 1 5ED305A4
P 6350 3975
F 0 "R6" H 6350 4325 50  0000 C CNN
F 1 "5k" H 6350 4200 50  0000 C CNN
F 2 "fab:R_1206" V 6280 3975 50  0001 C CNN
F 3 "~" H 6350 3975 50  0001 C CNN
	1    6350 3975
	1    0    0    -1  
$EndComp
Wire Wire Line
	7025 3725 7025 4325
Wire Wire Line
	6650 3925 8125 3925
Wire Wire Line
	8125 3825 6350 3825
Connection ~ 6350 3825
Wire Wire Line
	5775 3825 6350 3825
Wire Wire Line
	6650 3925 6650 4125
Connection ~ 6650 3925
Wire Wire Line
	6350 4125 6650 4125
Connection ~ 6650 4125
Wire Wire Line
	6650 4125 6650 4325
Wire Wire Line
	6950 4325 7025 4325
Connection ~ 7025 4325
$Comp
L Device:C C8
U 1 1 5ED99428
P 10300 3525
F 0 "C8" H 10100 3475 50  0000 L CNN
F 1 "1uF" H 10100 3400 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 10338 3375 50  0001 C CNN
F 3 "~" H 10300 3525 50  0001 C CNN
	1    10300 3525
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3325 10300 3325
Wire Wire Line
	10300 3675 10300 4325
Connection ~ 10300 4325
Wire Wire Line
	10300 4325 10000 4325
Wire Wire Line
	10300 3375 10300 3325
$Comp
L fab:D_Schottky D1
U 1 1 5EDC23BE
P 2075 2750
F 0 "D1" H 2075 2534 50  0000 C CNN
F 1 "D_Schottky" H 1975 2625 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 2075 2750 50  0001 C CNN
F 3 "" H 2075 2750 50  0001 C CNN
	1    2075 2750
	-1   0    0    1   
$EndComp
$Comp
L fab:D_Schottky D2
U 1 1 5EDC38C5
P 3800 1100
F 0 "D2" H 3800 884 50  0000 C CNN
F 1 "D_Schottky" H 3800 975 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 3800 1100 50  0001 C CNN
F 3 "" H 3800 1100 50  0001 C CNN
	1    3800 1100
	-1   0    0    1   
$EndComp
$Comp
L Device:C C7
U 1 1 5ECFF4AA
P 3575 1250
F 0 "C7" H 3690 1296 50  0000 L CNN
F 1 "1uF" H 3690 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 3613 1100 50  0001 C CNN
F 3 "~" H 3575 1250 50  0001 C CNN
	1    3575 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1100 3575 1100
Connection ~ 3575 1100
Wire Wire Line
	3575 1100 3525 1100
Connection ~ 3575 1400
$EndSCHEMATC
